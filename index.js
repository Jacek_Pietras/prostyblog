/*jshint node: true*/
var express = require("express");
var app = express();
var router = express.Router();
var path = __dirname + "/views/";
var morgan = require("morgan");

app.use(morgan("dev"));
app.use("/jquery", express.static("bower_components/jquery/dist"));
app.use("/bootstrap", express.static("bower_components/bootstrap/dist"));
app.use("/angular", express.static("bower_components/angular"));
app.use("/angular-route", express.static("bower_components/angular-route"));

router.use(function (req, res, next) {
    console.log("/", req.method);
    next();
});

router.get("/", function (req, res) {
    res.sendFile(path + "index.html");
});

router.get("/about", function (req, res) {
    res.sendFile(path + "about.html");
});

router.get("/contact", function (req, res) {
    res.sendFile(path + "contact.html");
});

app.use("/", router);

app.use("*", function (req, res) {
    res.sendFile(path + "404.html");
});

app.listen(8000, function () {
    console.log("Aplikacja działa na porcie 8000");
});